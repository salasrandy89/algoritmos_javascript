// Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.

const numero = 42;
if (numero >= 10 && numero < 100 ){
    const numero_1 = parseInt(numero / 10);
    const numero_2 = parseInt(numero % 10);

    if ( (numero_1 % numero_2) == 0 ){
        console.log(`El primer digito es multiplo del segundo`);
    }

    if ( (numero_2 % numero_1) == 0 ){
        console.log(`El segundo digito es multiplo del primero`);
    }

} else{
    console.log('Ingrese un numero valido');
}
