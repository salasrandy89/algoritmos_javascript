
// Leer 10 números enteros, almacenarlos en un vector 
// y determinar en qué posiciones se encuentra el número mayor

numero_1   = 11;
numero_2   = 1;
numero_3   = 13;
numero_4   = 14;
numero_5   = 16;
numero_6   = 122;
numero_7   = 134;
numero_8   = 16;
numero_9   = 18;
numero_10  = 19;
let vector = [];

vector.push(numero_1);
vector.push(numero_2);
vector.push(numero_3);
vector.push(numero_4);
vector.push(numero_5);
vector.push(numero_6);
vector.push(numero_7);
vector.push(numero_8);
vector.push(numero_9);
vector.push(numero_10);

let numero_mayor = 0;
let posicion_numero = 0;
let contador = 0;

let buscar_mayor = vector.map(buscar => {
    if( buscar > numero_mayor ){
        numero_mayor = buscar;
        posicion_numero = contador;
    }
    contador += 1;

});
console.log(`El numero mayor es ${numero_mayor} esta en la posicion ${posicion_numero}`);
