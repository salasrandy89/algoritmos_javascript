// Leer un número entero y determinar cuantas veces tiene el digito 1

const numero = 4911;
let contador = 0;
if (numero > 0) {

    let array = numero.toString().split('').map( digito => {
        if ( parseInt(digito) == 1){
            contador += 1
        }
     } );
     console.log(`El numero ${numero} tiene repetido el numero 1 : ${contador} veces`);

} else {
   console.log('Ingrese un numero valido')
}


