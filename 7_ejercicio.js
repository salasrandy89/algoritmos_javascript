// Leer un número entero de dos digitos y determinar si los dos digitos son iguales.

const numero = 11;
if (numero >= 10 && numero < 100 ){
    const numero_1 = parseInt(numero / 10);
    const numero_2 = parseInt(numero % 10);
    if ( numero_1 == numero_2 ){
        console.log(`Los digitos del numero ${numero} son iguales`);
    } else {
        console.log(`Los digitos del numero ${numero} no son iguales`);
    }
} else{
    console.log('Ingrese un numero valido');
}
