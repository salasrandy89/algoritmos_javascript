// Leer un número entero de dos digitos menor gue 20 y determinar si es primo.

const numero = 11;

if ( numero > 10 && numero < 20 ){

    for( let i = 2; i < numero ; i++ ){
        if ( ( numero % i) == 0 ){
            console.log(` ${numero} no es un numero primo`);
            return;
        }       
    }
    console.log(` ${numero} es un numero primo`);
    return;

} else {
    console.log('Ingrese un numero valido');
}