// Leer un número entero de dos digitos y determinar si ambos digitos son pares

const numero = 26;

if (numero >= 10 && numero < 100 ){

    const numero_1 = parseInt(numero / 10);
    const numero_2 = parseInt(numero % 10);

    if ( (numero_1 % 2) == 0 && (numero_2 % 2) == 0 ){
        console.log(`Los numeros ${numero_1} y ${numero_2} son pares`);
    }else{
        console.log('Ambos numero no son pares')
    }

} else{
    console.log('Ingrese un numero valido');
}