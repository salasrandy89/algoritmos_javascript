// Leer un número entero y determinar a cuánto es igual la suma de sus digitos.

const numero = 4976;
let suma = 0;
if (numero > 0) {

    let array = numero.toString().split('').map( digito => {
        suma = suma + parseInt(digito)
     } );
     console.log(`La suma de todos los digitos del numero ${numero} es ${suma}`);

} else {
   console.log('Ingrese un numero valido')
}


