// Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo. 

const numero = 3118;

if (numero >= 1000 && numero < 10000 ){

    const numero_1 = parseInt(numero / 1000);
    const numero_2 = parseInt((numero % 1000) / 100);
    const numero_3 = parseInt( ((numero % 1000) % 100) / 10);
    const numero_4 = parseInt( (((numero % 1000) % 100) % 10) );

    if ( numero_2 == numero_3 ){
        console.log(`El segundo digito y el penultimo digito del numero ${numero} son iguales`);
    } else {
        console.log(`No son iguales`);
    }

} else{
    console.log('Ingrese un numero valido');
}
