// Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos.

const numero = 13;
if (numero >= 10 && numero < 100 ){
    const numero_1 = parseInt(numero / 10);
    const numero_2 = parseInt(numero % 10);
    const suma = numero_1 + numero_2;
    console.log(`La suma de ${numero_1} + ${numero_2} = ${suma}`);

} else{
    console.log('Ingrese un numero valido');
}
